/// Pasians
/// ICP FIT VUT Brno 2017
///
/// @author Jakub Semrič, xsemri00@stud.fit.vutbr.cz
/// @author Peter Rusiňák, xrusin03@stud.fit.vutbr.cz
///
/// Implementation of @Card member functions.
/// @file card.cpp
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0
///

#include <QGraphicsObject>
#include <QPainter>
#include <QString>
#include <QGraphicsSceneMouseEvent>
#include <QPixmap>
#include <QTimer>
#include "table.h"
#include "pile.h"
#include "card.h"
#include "../core/playingcard.h"

/// Constructs a card and sets flags which enables moving with the card.
Card::Card(int cl, unsigned char num, QString path,
           QGraphicsObject *parent):
    QGraphicsObject{parent}, PlayingCard{cl, num, false}, fname{path},
    upperCard{0}, bottomCard{0}, pile{0}, pointUp{0}, ticks{0}
{
    setFlag(ItemIsSelectable);
    setFlag(ItemIsMovable);
    setCacheMode(DeviceCoordinateCache);
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(blink()));
    timer->setInterval(100);
}

/// Returns a card type as specific @QGraphicItem object.
int Card::type() const {
    return CardType;
}

/// Sets a pile where card ought to be located.
void Card::setPileRaw(Pile *p) {
    pile = p;
}

void Card::blink() {
    if (ticks == 3) {
        timer->stop();
        ticks = 0;
        setVisible(true);
        pointUp = false;
    }
    else {
        ticks++;
        setVisible(pointUp);
        pointUp = !pointUp;
    }
    update();
}

void Card::highlight() {
    if (!timer->isActive()) {
        timer->start(200);
        if (upperCard) {
            upperCard->highlight();
        }
    }
}

/// Sets a pile where card ought to be located and also
/// changes it's previous pile card stack. If card is not at top
/// then the pointer to a pile of cards beyond this card is also set.
void Card::setPile(Pile *p) {

    if (pile) {
        pile->removeCard(this);
    }

    pile = p;
    p->addCard(this);
}

/// Makes a card turned down so it is no more visible.
void Card::turnDown() {
    PlayingCard::turnDown();
    update();
}

/// Makes a card turned up so it is visible.
void Card::turnUp() {
    PlayingCard::turnUp();
    update();
}

/// Returns a pile where card is actually located.
Pile *Card::getPile() const {
    return pile;
}

/// Sets a pointer to a bottom card.
void Card::setBottomCard(Card *s) {
    bottomCard = s;
}

/// Sets a pointer to a upper card.
void Card::setUpperCard(Card *p) {
    upperCard = p;
}

/// Returns a pointer to a bottom card.
Card *Card::getBottomCard() const {
    return bottomCard;
}

/// Returns a pointer to a upper card.
Card *Card::getUpperCard() const {
    return upperCard;
}

Card::~Card() {}

QPainterPath Card::shape() const {
    QPainterPath p;
    p.addRect(boundingRect());
    return p;
}

/// Returns a rectangular shape of the card.
QRectF Card::boundingRect() const {
    return QRect(0,0,CARD_WIDTH, CARD_HEIGHT);
}

/// Paints the card with a corresponding picture.
void Card::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                 QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    QPixmap px;
    if (isTurnedUp()) {
        px = QPixmap(fname);

    }
    else {
        px = QPixmap(":/images/back.png");
    }

    px = px.scaled(CARD_WIDTH, CARD_HEIGHT);
    painter->setRenderHint(QPainter::Antialiasing, true);
    painter->setBrush(QBrush(px));
    //painter->setPen(Qt::SolidLine);
    painter->setPen(QColor(Qt::black));
    painter->drawRoundedRect(boundingRect(), 5, 5);
}

/// Changes Z-coordinate of card and its upper cards to maximal value among all items.
/// This provides that cards are always visible.
void Card::moveTop() {
    qreal maxZ = (qobject_cast<Table*>(scene()))->getMaxZCoord();

    // update Z-coordinate for card ant its upper cards
    for (Card *c = this; c != 0; c = c->upperCard) {
        maxZ += 0.1;
        c->setZValue(maxZ);
    }

}

/// Press the card, and initialize the position of move. Only visible cards can be
/// pressed.
void Card::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent) {
    if (isTurnedUp()) {
        mouseEvent->scenePos();
        pointerPos = mouseEvent->scenePos();
        moveTop();
    }
}

/// Moves with card and its upper cards.
void Card::mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent) {

    if (isTurnedUp() && pile->type() != Pile::Stock) {
        QPointF currentPos = mouseEvent->scenePos();
        qreal xdiff = currentPos.x() - pointerPos.x();
        qreal ydiff = currentPos.y() - pointerPos.y();;
        moveBy(xdiff, ydiff);
        pointerPos = currentPos;

        // move with all upper cards
        for (Card *c = upperCard; c != 0; c = c->upperCard) {
            c->moveBy(xdiff, ydiff);
        }
    }
}

/// Return to position before moving of the card including its upper cards.
/// Also decreases value of Z-coordinate.
void Card::cancelMove() {

    qreal x,y,z;
    x = pile->pos().x();
    y = pile->pos().y();

    if (getPile()->type() == Pile::CardDeck) {

        if (bottomCard) {
            y = bottomCard->pos().y() + CARD_DISTANCE;
            z = bottomCard->zValue();
        }
        else {
            // no bottom card ==> pile is empty
            z = pile->zValue();
        }

        // handle card and its upper cards
        for (Card *c = this; c; c = c->upperCard) {
            z += 0.1;
            c->setPos(x, y);
            c->setZValue(z);
            y += CARD_DISTANCE;
        }

    }
    else {
        // return back to stock/foundation position with top z value
        // only one card can be moved at once, no need to move upper cards
        z = pile->isEmpty() ? pile->zValue() : pile->getLast()->zValue();
        z += 0.1;
        setPos(x,y);
        setZValue(z);
    }
}


/// Moves card and its upper card to specific pile.
/// @param pile where cards will be appended
void Card::moveTo(Pile *destPile) {

    // remove bottom linkage
    if (bottomCard) {
        bottomCard->setUpperCard(0);
    }

    // new coordinates
    qreal x, y, z;
    x = destPile->x();
    y = destPile->y();
    z = destPile->isEmpty() ? destPile->zValue() :
                              destPile->getLast()->zValue();
    z += 0.1;

    if (destPile->type() == Pile::CardDeck) {
        // no card in pile
        if (destPile->isEmpty()) {
            setBottomCard(0);
        }
        else {
            // link with upper card of pile
            destPile->getLast()->setUpperCard(this);
            setBottomCard(destPile->getLast());
            y = destPile->getLast()->y() + CARD_DISTANCE;
        }

        // handle card and its upper cards
        for (Card *c = this; c; c = c->upperCard) {
            c->setPos(x, y);
            c->setZValue(z);
            c->setPile(destPile);
            y += CARD_DISTANCE;
            z += 0.1;
        }
    }
    // waste or foundation
    else {
        setPile(destPile);
        setBottomCard(0);
        setPos(x,y);
        setZValue(z);
    }

}
