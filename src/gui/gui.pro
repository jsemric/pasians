#-------------------------------------------------
#
# Project created by QtCreator 2017-04-01T20:00:56
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = hra2017
TEMPLATE = app

INCLUDEPATH += ../core

CONFIG += c++11

OBJECTS += ../core/*.o

SOURCES += main.cpp\
        mainwindow.cpp \
    card.cpp \
    table.cpp \
    subgame.cpp \
    pile.cpp \
    dialogs.cpp

HEADERS  += mainwindow.h \
    card.h \
    table.h \
    subgame.h \
    pile.h \
    dialogs.h

FORMS    +=

RESOURCES += \
    images.qrc
