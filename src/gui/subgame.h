/// Pasians
/// ICP FIT VUT Brno 2017
///
/// @author Jakub Semrič, xsemri00@stud.fit.vutbr.cz
/// @author Peter Rusiňák, xrusin03@stud.fit.vutbr.cz
///
/// Definition of class @SubGame.
/// @file subgame.h
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0
///

#ifndef SubGame_H
#define SubGame_H

#include <QMainWindow>
#include <QMenu>
#include <QAction>
#include <QGraphicsView>

class Table;
class WinDialog;

/// Represents a game.
class SubGame : public QMainWindow
{
    Q_OBJECT

private:
    /// Graphic view which user can see.
    QGraphicsView *view;
    /// The game scene.
    Table *table;

    QAction *exitAction;
    QAction *newTableAction;
    QAction *saveAction;
    QAction *loadAction;
    QAction *leaveAction;

    QAction *undoAction;
    QAction *helpAction;
    QAction *serveAction;

    QMenu *actionMenu;
    QMenu *gameMenu;

    WinDialog *winDialog;

public:
    /// Flag if the game is active.
    bool isPlayed;


public:
    explicit SubGame(QWidget *parent = 0, bool play = false);
    ~SubGame();

    void startGame();
    void disableNewTable();
    void enableNewTable();
    void victory();

private:
    void createMenus();
    void createActions();
    void resizeEvent(QResizeEvent *event);

public slots:
    void leaveGame();
    void restartGame();
    void undo();
    void save();
    void load();
};

#endif // SubGame_H
