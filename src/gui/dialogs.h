/// Pasians
/// ICP FIT VUT Brno 2017
///
/// @author Jakub Semrič, xsemri00@stud.fit.vutbr.cz
/// @author Peter Rusiňák, xrusin03@stud.fit.vutbr.cz
///
/// Definition of @WinDialog class which is displayed after winning the game.
/// @file dialogs.h
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0
///

#ifndef DIALOGS_H
#define DIALOGS_H

#include <QDialog>

class WinDialog : public QDialog
{
public:
    explicit WinDialog(QWidget *parent = 0);
    ~WinDialog() = default;
};

#endif // DIALOGS_H
