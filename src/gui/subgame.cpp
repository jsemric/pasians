/// Pasians
/// ICP FIT VUT Brno 2017
///
/// @author Jakub Semrič, xsemri00@stud.fit.vutbr.cz
/// @author Peter Rusiňák, xrusin03@stud.fit.vutbr.cz
///
/// Implementation of @SubGame member functions.
/// @file subgame.cpp
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0
///

#include <QGraphicsView>
#include <QMenu>
#include <QMenuBar>
#include <QFileDialog>
#include <QMessageBox>
#include "table.h"
#include "subgame.h"
#include "dialogs.h"

/// Construct a SubGame objects and sets everything to be ready to play.
SubGame::SubGame(QWidget *parent, bool play):
    QMainWindow(parent), isPlayed{play}
{
    table = new Table(play, this);
    table->setItemIndexMethod(QGraphicsScene::NoIndex);

    view = new QGraphicsView(table);
    view->setDragMode(QGraphicsView::NoDrag);
    view->setCacheMode(QGraphicsView::CacheBackground);
    view->setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
    view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view->setEnabled(play);
    setCentralWidget(view);

    winDialog = new WinDialog(this);

    createActions();
    createMenus();
    menuBar()->hide();
    setMinimumSize(500,350);
}

SubGame::~SubGame() { }

void SubGame::createActions() {

    // setting general actions
    exitAction = new QAction("Exit Game", this);
    connect(exitAction, SIGNAL (triggered()),this->parent(), SLOT (close()));

    newTableAction = new QAction("New Table", this);
    connect(newTableAction, SIGNAL (triggered()), this->parent(),
            SLOT (addTable()));


    leaveAction = new QAction("Leave Game", this);
    connect(leaveAction, SIGNAL (triggered()),this, SLOT (leaveGame()));
    connect(leaveAction, SIGNAL (triggered()),this->parent(),
            SLOT (subGameLeft()));


    serveAction = new QAction("Serve again", this);
    connect(serveAction, SIGNAL (triggered()), this, SLOT (restartGame()));

    saveAction = new QAction("Save", this);
    connect(saveAction, SIGNAL (triggered()), this, SLOT (save()));

    loadAction = new QAction("Load", this);
    connect(loadAction, SIGNAL (triggered()), this, SLOT (load()));

    undoAction = new QAction("Undo", this);
    connect(undoAction, SIGNAL (triggered()), this, SLOT (undo()));

    helpAction = new QAction("Help", this);
    connect(helpAction, SIGNAL (triggered()), table, SLOT (helpMove()));
}

/// Resizes a window in which a game is located.
void SubGame::resizeEvent(QResizeEvent *event) {

    table->setSceneRect(view->rect());

    if (isPlayed) {
        table->updatePositions(view->rect());
    }

    QWidget::resizeEvent(event);
}

void SubGame::createMenus() {

    gameMenu = menuBar()->addMenu("Game");
    actionMenu = menuBar()->addMenu("Commands");

    gameMenu->addAction(exitAction);
    gameMenu->addAction(newTableAction);
    gameMenu->addAction(leaveAction);
    gameMenu->addAction(saveAction);
    gameMenu->addAction(loadAction);

    actionMenu->addAction(serveAction);
    actionMenu->addAction(undoAction);
    actionMenu->addAction(helpAction);
}

void SubGame::startGame() {
    isPlayed = true;
    menuBar()->show();
    view->setEnabled(true);
    table->build();
    table->serveCards();
    table->updatePositions(view->rect());
}

void SubGame::leaveGame() {
    isPlayed = false;
    menuBar()->hide();
    table->leave();
    view->setEnabled(false);
}

void SubGame::restartGame() {
    table->clear();
    table->serveCards();
    table->updatePositions(view->rect());
}

void SubGame::disableNewTable() {
    newTableAction->setEnabled(false);
}

void SubGame::enableNewTable() {
    newTableAction->setEnabled(true);
}

void SubGame::undo() {
    if (table->validUndo()) {
        table->undo();
    }
}

void SubGame::victory() {
    winDialog->exec();
}

void SubGame::save() {
    QString saveFile = QFileDialog::getSaveFileName(this, tr("Save File"),
                                                    "examples");
    try {
        table->save(saveFile);
    }
    catch (...) {
        QMessageBox msg;
        msg.critical(0, "Error", "Cannot save game.");
    }
}

void SubGame::load() {
    QString loadFile = QFileDialog::getOpenFileName(this, tr("Load File"),
                                                    "examples");
    try {
        table->load(loadFile);
        table->updatePositions(view->rect());
    }
    catch (...) {
        QMessageBox msg;
        msg.critical(0, "Error", "Cannot load game. Serving a new game.");
        restartGame();
    }
}
