/// Pasians
/// ICP FIT VUT Brno 2017
///
/// @author Jakub Semrič, xsemri00@stud.fit.vutbr.cz
/// @author Peter Rusiňák, xrusin03@stud.fit.vutbr.cz
///
/// Implementation of member funcitons of class @Table.
/// @file table.cpp
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0
///

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QList>
#include <QVector>
#include <ctime>
#include <algorithm>
#include <cstdlib>
#include "table.h"
#include "pile.h"
#include "card.h"
#include "subgame.h"
#include "../core/command_manager.h"
#include "../core/commands.h"
#include "../core/save_load.h"

/// Constructs a new table and set its to blank scene or scene with cards.
Table::Table(bool active, QWidget *parent): QGraphicsScene{parent}
{
    if (active) {
        build();
        serveCards();
    }
    else {
        setBackgroundBrush(Qt::black);
    }
}

Table::~Table() { }

/// Dynamicaly alocates space for piles.
void Table::createPiles() {

    int ZCoord{0};

    // alocation of new piles
    for (int i = 0; i < 7; i++) {
        tableau[i] = new Pile(Pile::CardDeck);
        tableau[i]->setZValue(ZCoord);
        addItem(tableau[i]);
    }

    for (int i = 0; i < 4; i++) {
        foundations[i] = new Pile(Pile::Foundation);
        foundations[i]->setZValue(ZCoord);
        addItem(foundations[i]);
    }

    stock = new Pile(Pile::Stock);
    stock->setZValue(ZCoord);
    addItem(stock);

    wastePile = new Pile(Pile::Waste);
    wastePile->setZValue(ZCoord);
    addItem(wastePile);
}

/// Shuffles cards and append them to tableau and stock. It is called after
/// Starting and either restarting a game/subgame.
void Table::serveCards() {

    qreal ZCoord{0};

    QList<Card*> tmplist = shuffleCards();

    // adding cards to piles
    // cards are not owned by piles, they just containt poiters to them
    for (int i = 0; i < 7; i++) {
        Card *previousCard{0};
        for (int j = 0; j <= i; j++) {
            Card *card = tmplist.first();
            card->setZValue(++ZCoord);
            card->setPile(tableau[i]);

            // link cards with Bottom/Upper relationship
            if (previousCard) {
                previousCard->setUpperCard(card);
                card->setBottomCard(previousCard);
            }

            tmplist.removeFirst();
            previousCard = card;
        }
        previousCard->turnUp();
        ZCoord = 0;
    }

    Card *card;
    foreach (card, tmplist) {
        card->setZValue(++ZCoord);
        card->setPile(stock);
    }
}

/// Alocates space fo cards and piles, also creates a history of commands.
void Table::build() {
    setBackgroundBrush(Qt::yellow);
    cmdHistory = new CommandManager<Card,Pile>;
    createCards();
    createPiles();
}

/// Creates 52 cards without jokers.
void Table::createCards() {
    cards.append(new Card(PlayingCard::CLUB, 1,":/images/clubA.png"));
    cards.append(new Card(PlayingCard::CLUB, 2,":/images/club2.png"));
    cards.append(new Card(PlayingCard::CLUB, 3,":/images/club3.png"));
    cards.append(new Card(PlayingCard::CLUB, 4,":/images/club4.png"));
    cards.append(new Card(PlayingCard::CLUB, 5,":/images/club5.png"));
    cards.append(new Card(PlayingCard::CLUB, 6,":/images/club6.png"));
    cards.append(new Card(PlayingCard::CLUB, 7,":/images/club7.png"));
    cards.append(new Card(PlayingCard::CLUB, 8,":/images/club8.png"));
    cards.append(new Card(PlayingCard::CLUB, 9,":/images/club9.png"));
    cards.append(new Card(PlayingCard::CLUB, 10,":/images/club10.png"));
    cards.append(new Card(PlayingCard::CLUB, 11,":/images/clubJ.png"));
    cards.append(new Card(PlayingCard::CLUB, 12,":/images/clubQ.png"));
    cards.append(new Card(PlayingCard::CLUB, 13,":/images/clubK.png"));

    cards.append(new Card(PlayingCard::SPADE, 1,":/images/spadeA.png"));
    cards.append(new Card(PlayingCard::SPADE, 2,":/images/spade2.png"));
    cards.append(new Card(PlayingCard::SPADE, 3,":/images/spade3.png"));
    cards.append(new Card(PlayingCard::SPADE, 4,":/images/spade4.png"));
    cards.append(new Card(PlayingCard::SPADE, 5,":/images/spade5.png"));
    cards.append(new Card(PlayingCard::SPADE, 6,":/images/spade6.png"));
    cards.append(new Card(PlayingCard::SPADE, 7,":/images/spade7.png"));
    cards.append(new Card(PlayingCard::SPADE, 8,":/images/spade8.png"));
    cards.append(new Card(PlayingCard::SPADE, 9,":/images/spade9.png"));
    cards.append(new Card(PlayingCard::SPADE, 10,":/images/spade10.png"));
    cards.append(new Card(PlayingCard::SPADE, 11,":/images/spadeJ.png"));
    cards.append(new Card(PlayingCard::SPADE, 12,":/images/spadeQ.png"));
    cards.append(new Card(PlayingCard::SPADE, 13,":/images/spadeK.png"));

    cards.append(new Card(PlayingCard::DIAMOND, 1,":/images/diamondA.png"));
    cards.append(new Card(PlayingCard::DIAMOND, 2,":/images/diamond2.png"));
    cards.append(new Card(PlayingCard::DIAMOND, 3,":/images/diamond3.png"));
    cards.append(new Card(PlayingCard::DIAMOND, 4,":/images/diamond4.png"));
    cards.append(new Card(PlayingCard::DIAMOND, 5,":/images/diamond5.png"));
    cards.append(new Card(PlayingCard::DIAMOND, 6,":/images/diamond6.png"));
    cards.append(new Card(PlayingCard::DIAMOND, 7,":/images/diamond7.png"));
    cards.append(new Card(PlayingCard::DIAMOND, 8,":/images/diamond8.png"));
    cards.append(new Card(PlayingCard::DIAMOND, 9,":/images/diamond9.png"));
    cards.append(new Card(PlayingCard::DIAMOND, 10,":/images/diamond10.png"));
    cards.append(new Card(PlayingCard::DIAMOND, 11,":/images/diamondJ.png"));
    cards.append(new Card(PlayingCard::DIAMOND, 12,":/images/diamondQ.png"));
    cards.append(new Card(PlayingCard::DIAMOND, 13,":/images/diamondK.png"));

    cards.append(new Card(PlayingCard::HEART, 1,":/images/heartA.png"));
    cards.append(new Card(PlayingCard::HEART, 2,":/images/heart2.png"));
    cards.append(new Card(PlayingCard::HEART, 3,":/images/heart3.png"));
    cards.append(new Card(PlayingCard::HEART, 4,":/images/heart4.png"));
    cards.append(new Card(PlayingCard::HEART, 5,":/images/heart5.png"));
    cards.append(new Card(PlayingCard::HEART, 6,":/images/heart6.png"));
    cards.append(new Card(PlayingCard::HEART, 7,":/images/heart7.png"));
    cards.append(new Card(PlayingCard::HEART, 8,":/images/heart8.png"));
    cards.append(new Card(PlayingCard::HEART, 9,":/images/heart9.png"));
    cards.append(new Card(PlayingCard::HEART, 10,":/images/heart10.png"));
    cards.append(new Card(PlayingCard::HEART, 11,":/images/heartJ.png"));
    cards.append(new Card(PlayingCard::HEART, 12,":/images/heartQ.png"));
    cards.append(new Card(PlayingCard::HEART, 13,":/images/heartK.png"));

    Card *card;
    foreach (card, cards) {
        addItem(card);
    }
}

/// Provides undo operations.
void Table::undo() {
    possibleMoves.clear();
    cmdHistory->undo();
}

/// Checks if stack of operations is empty.
bool Table::validUndo() {
    return !cmdHistory->isEmpty();
}

/// Updates position of all items in table. Used when window is beeing resized.
/// Also set start positions of all objects.
void Table::updatePositions(const QRectF &rect) {

    // will be used latter, maybe
    Q_UNUSED(rect);

    // localization of piles
    wastePile->setPos(CARD_WIDTH, 0);
    stock->setPos(0,0);

    qreal cordX{3*CARD_WIDTH};
    qreal cordY;

    for (int i = 0; i < 4; i++) {
        cordX += CARD_WIDTH;
        foundations[i]->setPos(cordX, 0);
    }

    cordX = 5;
    for (int i = 0; i < 7; i++) {
        tableau[i]->setPos(cordX, CARD_HEIGHT + CARD_DISTANCE);
        cordX += PILE_DISTANCE;
    }

    // dealing with cards in piles
    Card *card;
    for (int i = 0; i < 7; i++) {
        cordX = tableau[i]->x();
        cordY = tableau[i]->y();
        foreach (card, tableau[i]->cards) {
            card->setPos(cordX, cordY);
            cordY += CARD_DISTANCE;
        }
    }

    for (int i = 0; i < 4; i++) {
        cordX = foundations[i]->x();
        cordY = foundations[i]->y();
        foreach (card, foundations[i]->cards) {
            card->setPos(cordX, cordY);
        }
    }

    foreach (card, stock->cards) {
        card->setPos(stock->x(), stock->y());
    }

    foreach (card, wastePile->cards) {
        card->setPos(wastePile->x(), wastePile->y());
    }
}

/// Clears all link between items in graphics scene. So the table can be reused for
/// new play.
void Table::clear() {
    // remove link between cards
    Card *card;
    foreach (card, cards) {
        card->setBottomCard(0);
        card->setUpperCard(0);
        card->turnDown();
    }

    // remove cards from piles
    for (int i = 0; i < 7; i++) {
        tableau[i]->cards.clear();
    }

    for (int i = 0; i < 4; i++) {
        foundations[i]->cards.clear();
    }

    stock->cards.clear();
    wastePile->cards.clear();
    possibleMoves.clear();

    // reset cmd history
    delete cmdHistory;
    cmdHistory = new CommandManager<Card, Pile>;
}

/// Removes all items in scene (cards and piles) and removes command history. After
/// removing all items the scene is set to black rectangle.
void Table::leave() {
    QGraphicsScene::clear();
    cards.clear();
    delete cmdHistory;
    setBackgroundBrush(Qt::black);
}

/// Shuffling cards.
/// @return Returns shuffled list of pointers to cards.
QList<Card*> Table::shuffleCards() {
    QVector<Card*> ret = QVector<Card*>::fromList(cards);

    std::srand (unsigned(std::time(0)));
    std::random_shuffle (ret.begin(), ret.end(), [](int i) {
         return (std::rand() + i) % 52; });

    return QList<Card*>::fromVector(ret);
}

/// Search for the item in scene with highest Z-coordinate value and returns
/// this accquired value.
qreal Table::getMaxZCoord() {
    qreal maxZ{0};
    Card *card;
    foreach (card, cards) {
        if (card->zValue() > maxZ) {
            maxZ = card->zValue();
        }
    }
    return maxZ;
}

/// Returns true if move is valid, false otherwise.
bool Table::isValidMove(Card *card, Pile *pile) {

    if (pile->type() == Pile::CardDeck) {

        if (pile->isEmpty()) {
            return card->getNumber() == KING;
        }
        else {
            Card *lastCard = pile->getLast();
            return (lastCard->isTurnedUp() &&
                    card->getNumber() + 1 == lastCard->getNumber() &&
                    card->isRed() != lastCard->isRed());
        }

    }
    else if (pile->type() == Pile::Foundation) {

        if (pile->isEmpty()) {
            return !card->getUpperCard() && card->getNumber() == ACE;
        }
        else {
            Card *lastCard = pile->getLast();
            return (!card->getUpperCard() &&
                    card->getColor() == lastCard->getColor() &&
                    card->getNumber() == lastCard->getNumber() + 1);
        }

    }

    return false;
}

void Table::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent) {

    activeItem = itemAt(mouseEvent->scenePos(), QTransform());

    if (activeItem && activeItem->type() == Pile::Stock) {
        fromStockToWaste();
    }

    QGraphicsScene::mousePressEvent(mouseEvent);
}

void Table::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *mouseEvent) {
    activeItem = itemAt(mouseEvent->scenePos(), QTransform());

    if (activeItem && activeItem->type() == Card::CardType) {
        Card *card = static_cast<Card*>(activeItem);
        if (card->isTurnedUp() && !card->getUpperCard()) {
            for (int i = 0; i < 4; i++) {
                if (isValidMove(card, foundations[i])) {
                    executeMove(new MoveCommand<Card, Pile>
                                (card, card->getPile(), foundations[i]));
                    checkWin();
                    break;
                }
            }
        }
    }

    QGraphicsScene::mouseDoubleClickEvent(mouseEvent);
}

void Table::mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent) {
    if (activeItem) {
        QGraphicsScene::mouseMoveEvent(mouseEvent);
    }
}

void Table::mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent) {

    QGraphicsScene::mouseReleaseEvent(mouseEvent);

    if (activeItem && activeItem->type() == Card::CardType) {

        Card *card = static_cast<Card*>(activeItem);

        // uncover card
        if (!card->isTurnedUp()) {

            if (card->getPile()->type() == Pile::CardDeck
                && !card->getUpperCard())
            {
                // turns card up, so it becames visible
                executeMove(new ClickCommand<Card, Pile>(card, 0, 0));
                return;
            }
            else if (card->getPile()->type() == Pile::Stock) {
                // moves a card from stock to waste
                executeMove(new ClickCommand<Card, Pile>
                            (0, stock, wastePile));
                return;
            }

        }
        else {

            // get colliding items with card
            QList<QGraphicsItem*> collidItems = collidingItems(activeItem);
            clearCollidingItems(collidItems);

            // collision with invalid stacking objects
            if (collidItems.empty()) {
                card->cancelMove();
            }
            else {
                // move is invalid, so return to previous position
                if(!moveCard(card, collidItems)) {
                    card->cancelMove();
                }
                else {
                    checkWin();
                }
            }
        }
    }

    activeItem = 0;
}

/// Removes all items which intersect with active item but are not necessary for
/// card move actions.
void Table::clearCollidingItems(QList<QGraphicsItem*> &collidItems) {

    QGraphicsItem *item;
    Card *activeCard = static_cast<Card*>(activeItem);

    foreach (item, collidItems) {
        // collides with card
        if (item->type() == Card::CardType) {

            Card *card = static_cast<Card*>(item);

            // remove all cards in waste or stock
            if (card->getPile()->type() == Pile::Waste ||
                card->getPile()->type() == Pile::Stock)
            {
                collidItems.removeOne(item);
            }
        }
        else if (item->type() == Pile::Stock || item->type() == Pile::Waste ||
                 static_cast<Pile*>(item) == activeCard->getPile())
        {
            collidItems.removeOne(item);
        }
    }
}

/// Moving with cards to different piles.
/// @param active item
/// @param colliding item with active item
/// @return Returns false if move is invalid, true otherwise.
bool Table::moveCard(Card *card, QList<QGraphicsItem*> &collidItems) {

    QGraphicsItem *item;

    foreach (item, collidItems) {

        // collides with card
        if (item->type() == Card::CardType) {
            Card *collidCard = static_cast<Card*>(item);
            Pile *pile = collidCard->getPile();

            // card with in the same pile
            /*if (pile == card->getPile()) {
                return false;
            }*/

            // is it possible to stack card to pile?
            if (pile->type() == Pile::CardDeck ||
                pile->type() == Pile::Foundation)
            {
                if (isValidMove(card, pile)) {
                    executeMove(new MoveCommand<Card, Pile>
                                (card, card->getPile(), pile));
                    return true;
                }
            }
        }
        else if (item->type() == Pile::Foundation ||
                 item->type() == Pile::CardDeck)
        {
            Pile *pile = static_cast<Pile*>(item);
            if (isValidMove(card, pile)) {
                executeMove(new MoveCommand<Card, Pile>
                            (card, card->getPile(), pile));
                return true;
            }
        }
    }

    return false;
}

/// Moves a card from stock to waste pile. If stock is empty then all cards
/// from waste are moved back to stock.
void Table::fromStockToWaste() {

    // if both piles are empty, then do nothing
    if (!stock->isEmpty() || !wastePile->isEmpty()) {
        // moves a card from stock to waste
        executeMove(new ClickCommand<Card, Pile>(0, stock, wastePile));
    }

}

/// Determines if the actual state of the game is victory state.
void Table::checkWin() {

    if (foundations[0]->cards.size() == 13 &&
        foundations[1]->cards.size() == 13 &&
        foundations[2]->cards.size() == 13 &&
        foundations[3]->cards.size() == 13)
    {
        // call dialog window
        static_cast<SubGame*>(parent())->victory();
    }

}

void Table::executeMove(Command<Card, Pile> *cmd) {
    cmdHistory->execute(cmd);
    possibleMoves.clear();
}

/// Provides a help action which moves or uncovers cards.
void Table::helpMove() {
    static unsigned cnt = 0;
    if (possibleMoves.empty()) {
        cnt = 0;
        possibleMoves = helpAction<Card,Pile>(wastePile, foundations, tableau);
    }

    if (!possibleMoves.empty()) {
        if (possibleMoves[cnt].second) {
            possibleMoves[cnt].second->highlight();
        }

        possibleMoves[cnt].first->highlight();
        if (++cnt == possibleMoves.size()) {
            cnt = 0;
        }
    }
}

/// Saves the actual state of the game to a specified file.
void Table::save(QString file) {
    save_cards<Card, Pile>(foundations, stock, wastePile, tableau,
                           file.toUtf8().constData());
}

/// Loads the state of the game from a specified file.
void Table::load(QString file) {

    clear();
    load_cards<QList<Card*>, Card, Pile>(cards, foundations, stock, wastePile,
                                         tableau, file.toUtf8().constData());

    qreal ZCoord{0};
    Card *card;

    // set links between cards in tableau piles
    for (int i = 0; i < 7; i++) {
        Card *previousCard{0};
        foreach (card, tableau[i]->cards) {
            card->setZValue(++ZCoord);
            card->setPileRaw(tableau[i]);

            // link cards with Bottom/Upper relationship
            if (previousCard) {
                previousCard->setUpperCard(card);
                card->setBottomCard(previousCard);
            }

            previousCard = card;
        }
        ZCoord = 0;
    }

    for (int i = 0; i < 4; i++) {
        foreach (card, foundations[i]->cards) {
            card->setPileRaw(foundations[i]);
            card->setZValue(++ZCoord);
            card->turnUp();
        }
        ZCoord = 0;
    }

    foreach (card, stock->cards) {
        card->setPileRaw(stock);
        card->setZValue(++ZCoord);
    }
    ZCoord = 0;

    foreach (card, wastePile->cards) {
        card->setPileRaw(wastePile);
        card->setZValue(++ZCoord);
        card->turnUp();
    }

}
