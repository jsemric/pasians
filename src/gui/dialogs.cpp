/// Pasians
/// ICP FIT VUT Brno 2017
///
/// @author Jakub Semrič, xsemri00@stud.fit.vutbr.cz
/// @author Peter Rusiňák, xrusin03@stud.fit.vutbr.cz
///
/// Implementation of @WinDialog member functions.
/// @file dialogs.cpp
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0
///

#include <QDialog>
#include <QPushButton>
#include "dialogs.h"

WinDialog::WinDialog(QWidget *parent) : QDialog(parent)
{
    setFixedSize(400,120);
    // last flag stands for disabling close button
    setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint);
    setWindowTitle("Heroic victory!");

    // button to serve cards
    QPushButton *serveButton = new QPushButton(this);
    serveButton->setText("Serve again");
    serveButton->setGeometry(50,45,120,40);
    connect(serveButton, SIGNAL (released()), this->parent(),
            SLOT (restartGame()));
    connect(serveButton, SIGNAL (released()), this, SLOT (close()));

    // button for exiting the game
    QPushButton *leaveButton = new QPushButton(this);
    leaveButton->setText("Leave game");
    leaveButton->setGeometry(50 + 120 + 40, 45, 120, 40);
    connect(leaveButton, SIGNAL (released()), this->parent(),
            SLOT (leaveGame()));
    connect(leaveButton, SIGNAL (released()), this->parent()->parent(),
            SLOT (subGameLeft()));
    connect(leaveButton, SIGNAL (released()), this, SLOT (close()));
}
