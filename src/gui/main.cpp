/// Pasians
/// ICP FIT VUT Brno 2017
///
/// @author Jakub Semrič, xsemri00@stud.fit.vutbr.cz
/// @author Peter Rusiňák, xrusin03@stud.fit.vutbr.cz
///
/// Runs a game with graphics interface.
/// @file main.cpp
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0
///

#include <QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
