/// Pasians
/// ICP FIT VUT Brno 2017
///
/// @author Jakub Semrič, xsemri00@stud.fit.vutbr.cz
/// @author Peter Rusiňák, xrusin03@stud.fit.vutbr.cz
///
/// Defintion of class @Card.
/// @file card.h
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0
///

#ifndef CARDWIDGET_H
#define CARDWIDGET_H

#include <QGraphicsSceneMouseEvent>
#include <QGraphicsObject>
#include <QWidget>
#include <QString>
#include "../core/playingcard.h"

#define CARD_WIDTH 60
#define CARD_HEIGHT 90
#define CARD_DISTANCE 10

class Pile;
class QTimer;

/// Class for representing a playing card.
class Card : public QGraphicsObject, public PlayingCard
{
    Q_OBJECT

public:
    // for graphic item
    enum {CardType = UserType + 5};
private:
    /// path to a picture to draw
    QString fname;
    Card *upperCard;
    Card *bottomCard;
    /// pile where a card is located
    Pile *pile;
    QPointF pointerPos;
    bool pointUp;
    QTimer *timer;
    int ticks;

public:
    Card(int cl, unsigned char num, QString path,
         QGraphicsObject *parrent = 0);
    ~Card();

    void setPile(Pile *p);
    void setPileRaw(Pile *p);
    void setUpperCard(Card *p);
    void setBottomCard(Card *s);
    void highlight();
    void turnUp() override;
    void turnDown() override;
    int type() const;
    Pile *getPile() const;
    Card *getUpperCard() const;
    Card *getBottomCard() const;

    void moveTop();
    void moveTo(Pile *destPile);
    void cancelMove();

    void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget = Q_NULLPTR);
    QRectF boundingRect() const;
    QPainterPath shape() const;

public slots:
    void blink();

};

#endif // CARDWIDGET_H
