/// Pasians
/// ICP FIT VUT Brno 2017
///
/// @author Jakub Semrič, xsemri00@stud.fit.vutbr.cz
/// @author Peter Rusiňák, xrusin03@stud.fit.vutbr.cz
///
/// Implementation of @Pile member functions.
/// @file pile.cpp
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0
///

#include <QGraphicsObject>
#include <QGraphicsScene>
#include <QPainter>
#include <QList>
#include <QTimer>
#include "pile.h"
#include "card.h"
#include "../core/basepile.h"

Pile::Pile(int type, QGraphicsObject *parent):
    QGraphicsObject{parent}, itemType{type}, pointUp{0}, ticks{0}
{
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(blink()));
    timer->setInterval(100);
}

Pile::~Pile() {}

int Pile::type() const {
    return itemType;
}

bool Pile::isEmpty() const {
    return cards.empty();
}

QList<Card*> Pile::getCards() const {
    return cards;
}

/// Appends a poiter to a card to the pile list.
/// @param c Card to append.
void Pile::addCard(Card *c) {
    cards.append(c);
}

/// Removes a pointer to a card.
/// @param c Card to remove.
void Pile::removeCard(Card *c) {
    int pos = 0;
    for (auto i = cards.begin(); i != cards.end(); i++, pos++) {
        if (*i == c) {
            cards.removeAt(pos);
            break;
        }
    }
}

Card* Pile::getLast() {
    return cards.back();
}

QPainterPath Pile::shape() const {
    QPainterPath p;
    p.addRect(boundingRect());
    return p;
}

QRectF Pile::boundingRect() const {
    return QRect(0,0,CARD_WIDTH, CARD_HEIGHT);
}

void Pile::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                 QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->setPen(Qt::SolidLine);
    painter->setPen(QColor(Qt::black));
    QRectF r = boundingRect();
    r.adjust(4,4,-4,-4);
    painter->setBrush(Qt::darkYellow);
    painter->drawRoundedRect(r,2,2);
}

void Pile::blink() {
    if (ticks == 3) {
        timer->stop();
        ticks = 0;
        setVisible(true);
        pointUp = false;
    }
    else {
        ticks++;
        setVisible(pointUp);
        pointUp = !pointUp;
    }
    update();
}

void Pile::highlight() {
    if (isEmpty()) {
        if (!timer->isActive()) {
            timer->start(200);
        }
    }
    else {
        getLast()->highlight();
    }
}
