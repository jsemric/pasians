/// Pasians
/// ICP FIT VUT Brno 2017
///
/// @author Jakub Semrič, xsemri00@stud.fit.vutbr.cz
/// @author Peter Rusiňák, xrusin03@stud.fit.vutbr.cz
///
/// Implementation of @MainWindow member functions.
/// @file mainwindow.cpp
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0
///

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QWidget>
#include <QHBoxLayout>
#include <QMenu>
#include <QMenuBar>
#include <QAction>
#include <QApplication>
#include "mainwindow.h"
#include "subgame.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    tableCount = 1;
    s1 = new SubGame(this, true);
    s1->menuBar()->show();

    QWidget *centralWidget = new QWidget();

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(s1);
    centralWidget->setLayout(layout);
    menuBar()->hide();

    setCentralWidget(centralWidget);

    setWindowTitle("Solitaire");
}

MainWindow::~MainWindow() {}


/// Sets a four game layout, so it is possible to play more games at once.
void MainWindow::setFourGamesLayout() {

    s2 = new SubGame(this);

    s3 = new SubGame(this);

    s4 = new SubGame(this);

    QWidget *centralWidget = new QWidget();
    QHBoxLayout *hl1 = new QHBoxLayout;
    hl1->addWidget(s1);
    hl1->addWidget(s2);

    QHBoxLayout *hl2 = new QHBoxLayout;
    hl2->addWidget(s3);
    hl2->addWidget(s4);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addLayout(hl1);
    layout->addLayout(hl2);

    centralWidget->setLayout(layout);

    setCentralWidget(centralWidget);
}

/// Creates or activates a new game in the new window.
void MainWindow::addTable() {

    // changing layout - first change, hence 4 tables, 2 blank ones
    if (tableCount == 1) {
        setFourGamesLayout();
        tableCount++;
        s2->startGame();
        return;
    }

    // find first empty and sever cards
    if (!s1->isPlayed) {
        s1->startGame();
    }
    else if (!s2->isPlayed) {
        s2->startGame();
    }
    else if (!s3->isPlayed) {
        s3->startGame();
    }
    else {
        s4->startGame();
    }

    // disable new game option from menu bar
    tableCount++;
    if (tableCount == 4) {
        s1->disableNewTable();
        s2->disableNewTable();
        s3->disableNewTable();
        s4->disableNewTable();
    }

}

/// Decrease a count of active games. If no game left the whole application
/// quits. If just one game stayed left the layout is turned back to one
/// game layout.
void MainWindow::subGameLeft() {

    tableCount--;

    if (tableCount == 0) {
        QApplication::quit();
    }
    else if (tableCount == 1) {
        setOneGameLayout();
    }
    else {
        // enable addTable action
        s1->enableNewTable();
        s2->enableNewTable();
        s3->enableNewTable();
        s4->enableNewTable();
    }
}

/// Sets a one game layout, thus one game occupies whole window.
void MainWindow::setOneGameLayout() {

    // find active game and swap it with s1
    if (s2->isPlayed) {
        s1 = s2;
    }
    else if (s3->isPlayed) {
        s1 = s3;
    }
    else if (s4->isPlayed) {
        s1 = s4;
    }

    QWidget *centralWidget = new QWidget();
    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(s1);
    centralWidget->setLayout(layout);

    setCentralWidget(centralWidget);
}
