/// Pasians
/// ICP FIT VUT Brno 2017
///
/// @author Jakub Semrič, xsemri00@stud.fit.vutbr.cz
/// @author Peter Rusiňák, xrusin03@stud.fit.vutbr.cz
///
/// Definition of class @Table.
/// @file table.h
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0
///

#ifndef TABLE_H
#define TABLE_H

#include <QGraphicsScene>
#include <QWidget>
#include <QList>
#include <vector>

class QGraphicsItem;
class Card;
class Pile;
template<class CardType, class PileType> class Command;
template<class CardType, class PileType> class CommandManager;

/// Class which represents a game scene and game logic.
class Table: public QGraphicsScene
{
    Q_OBJECT

private:
    /// List of all cards.
    QList<Card*> cards;
    Pile *foundations[4];
    Pile *stock;
    Pile *wastePile;
    Pile *tableau[7];
    /// Item which is currently pressed or selected by user.
    QGraphicsItem *activeItem;
    CommandManager<Card,Pile> *cmdHistory;
    std::vector<std::pair<Card*,Pile*>> possibleMoves;

public:
    Table(bool active, QWidget *parent = 0);
    ~Table();

    void build();
    void serveCards();
    void clear();
    void leave();
    void undo();

    bool validUndo();
    qreal getMaxZCoord();

private:
    void createCards();
    void createPiles();
    QList<Card*> shuffleCards();

    void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent);
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *mouseEvent);

    bool isValidMove(Card *card, Pile *pile);
    bool moveCard(Card *card, QList<QGraphicsItem*> &collidItems);
    void fromStockToWaste();
    void checkWin();
    void clearCollidingItems(QList<QGraphicsItem*> &collidItems);
    void executeMove(Command<Card, Pile> *cmd);

public slots:
    void updatePositions(const QRectF &rect);
    void helpMove();
    void save(QString file);
    void load(QString file);
};

#endif // TABLE_H
