/// Pasians
/// ICP FIT VUT Brno 2017
///
/// @author Jakub Semrič, xsemri00@stud.fit.vutbr.cz
/// @author Peter Rusiňák, xrusin03@stud.fit.vutbr.cz
///
/// Definition of class @MainWindow.
/// @file mainwindow.h
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0
///

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QAction>
#include <QMenu>
#include <QWidget>

class SubGame;

/// Encapsulates the whole game.
class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    /// Games which can be simultaneously played.
    SubGame *s1, *s2, *s3, *s4;
    /// Number of active games.
    int tableCount;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    void setFourGamesLayout();
    void setOneGameLayout();

private slots:
    void addTable();
    void subGameLeft();
};

#endif // MAINWINDOW_H
