/// Pasians
/// ICP FIT VUT Brno 2017
///
/// @author Jakub Semrič, xsemri00@stud.fit.vutbr.cz
/// @author Peter Rusiňák, xrusin03@stud.fit.vutbr.cz
///
/// Definition of class @Pile.
/// @file pile.h
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0
///

#ifndef PILE_H
#define PILE_H

#include <QGraphicsScene>
#include <QGraphicsObject>
#include <QList>
#include <QObject>
#include <QGraphicsObject>

#define PILE_DISTANCE 65

class Card;
class QTimer;
class Table;

/// Represents a real world card deck in GUI.
class Pile : public QGraphicsObject
{
    Q_OBJECT

public:
    enum {Foundation = UserType + 10, CardDeck, Stock, Waste};
    friend class Table;

private:
    /// Type of a pile.
    int itemType;
    /// List of cards.
    QList<Card*> cards;
    QTimer *timer;
    bool pointUp;
    int ticks;

public:
    explicit Pile(int type, QGraphicsObject *parent = 0);
    ~Pile();

    // BasePile methods
    void addCard(Card *c);
    void removeCard(Card *c);
    Card* getLast();
    bool isEmpty() const;
    QList<Card*> getCards() const;
    void highlight();

    // QGraphicItem methods
    int type() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget = Q_NULLPTR);
    QRectF boundingRect() const;
    QPainterPath shape() const;

public slots:
    void blink();
};

#endif // PILE_H
