/// Pasians
/// ICP FIT VUT Brno 2017
///
/// @author Jakub Semrič, xsemri00@stud.fit.vutbr.cz
/// @author Peter Rusiňák, xrusin03@stud.fit.vutbr.cz
///
/// Definition of class @CliGame.
/// @file cli_game.h
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0
///

#ifndef CLI_GAME_H
#define CLI_GAME_H

#include <list>
#include <string>
#include <map>

class PlayingCard;
class BasePile;
template <class PlayingCard, class BasePile> class CommandManager;

/// Manages a game and interaction with user.
class CliGame {

public:
    enum  {Move = 0, Help, Print, Uncover, Get, Serve, Exit, Load,
           Save, Undo, HelpMove};

    /// Mapping commands to integers.
    static std::map<std::string, int> cmdList;

private:
    /// Stack of cards, ought to be 52.
    std::list<PlayingCard*> cards;
    /// Foundation piles.
    BasePile *foundations[4];
    /// Stock pile.
    BasePile *stock;
    /// Waste pile.
    BasePile *wastePile;
    /// Piles located in the playing scene.
    BasePile *tableau[7];
    /// Operations manager.
    CommandManager<PlayingCard, BasePile> *cmdHistory;

public:
    CliGame();
    ~CliGame();

    void play();

private:
    void displayCards();
    void setUpPiles();
    void clear();
    void showPossibleMoves();
    void load(const std::string &);
    void save(const std::string &);
    bool isValidMove(PlayingCard*, BasePile*,int) const;
    std::list<PlayingCard*> shuffleCards();
    bool checkWin() const;

};


#endif
