/// Pasians
/// ICP FIT VUT Brno 2017
///
/// @author Jakub Semrič, xsemri00@stud.fit.vutbr.cz
/// @author Peter Rusiňák, xrusin03@stud.fit.vutbr.cz
///
/// Runs a game.
/// @file main.cpp
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0
///

#include "cli_game.h"

int main() {

    CliGame game{};
    game.play();

    return 0;
}
