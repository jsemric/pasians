/// Pasians
/// ICP FIT VUT Brno 2017
///
/// @author Jakub Semrič, xsemri00@stud.fit.vutbr.cz
/// @author Peter Rusiňák, xrusin03@stud.fit.vutbr.cz
///
/// Implementation of command line version of the game, mainly the
/// user interface.
/// @file cli_game.cpp
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0
///

#include "cli_game.h"
#include "../core/basepile.h"
#include "../core/playingcard.h"
#include "../core/command_manager.h"
#include "../core/commands.h"
#include "../core/save_load.h"
#include <vector>
#include <list>
#include <ctime>
#include <cstdlib>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <sstream>
#include <map>

// Mapping a commands to some enumeration type.
std::map<std::string, int> CliGame::cmdList{
    {"move", CliGame::Move}, {"m", CliGame::Move},
    {"get", CliGame::Get}, {"g", CliGame::Get},
    {"uncover", CliGame::Uncover}, {"c", CliGame::Uncover},
    {"undo", CliGame::Undo}, {"u", CliGame::Undo},
    {"helpmove", CliGame::HelpMove}, {"hm", CliGame::HelpMove},
    {"help", CliGame::Help}, {"h", CliGame::Help},
    {"print", CliGame::Print}, {"p", CliGame::Print},
    {"again", CliGame::Serve}, {"a", CliGame::Serve},
    {"save", CliGame::Save}, {"s", CliGame::Save},
    {"load", CliGame::Load}, {"l", CliGame::Load},
    {"exit", CliGame::Exit}, {"e", CliGame::Exit}
};

static std::string helpStr = {
    "--------------------------------------------------------\n"
    "Commands:\n"
    "print p                  - prints a tableau\n"
    "help h                   - show this help\n"
    "move, m FROM WHERE [POS] - move card(s) between piles\n"
    "get, g                   - move card from stock to waste\n"
    "uncover, c FROM          - uncover card\n"
    "undo u                   - undo action\n"
    "helpmove hm              - help move\n"
    "again, a                 - serves a new game\n"
    "exit, e                  - exit game\n"
    "save, s FNAME            - save game\n"
    "load, l FNAME            - load game\n"
    "--------------------------------------------------------\n\n"
};

/// Creating a game and setting up all to play.
CliGame::CliGame() {

    int cnt = 0;
    for (int i = 0; i < 7; i++) {
        tableau[i] = new BasePile{cnt++};
    }

    for (int i = 0; i < 4; i++) {
        foundations[i] = new BasePile{cnt++};
    }

    stock = new BasePile{cnt++};
    wastePile = new BasePile{cnt++};

    for (unsigned char i = 1; i <= 13; i++) {
        cards.push_back(new PlayingCard{PlayingCard::SPADE, i, false});
        cards.push_back(new PlayingCard{PlayingCard::HEART, i, false});
        cards.push_back(new PlayingCard{PlayingCard::DIAMOND, i, false});
        cards.push_back(new PlayingCard{PlayingCard::CLUB, i, false});
    }

    cmdHistory = new CommandManager<PlayingCard, BasePile>();
}

CliGame::~CliGame() {

    for (int i = 0; i < 7; i++) {
        delete tableau[i];
    }

    for (int i = 0; i < 4; i++) {
        delete foundations[i];
    }

    delete stock;
    delete wastePile;

    while (!cards.empty()) {
        delete cards.back();
        cards.pop_back();
    }

    delete cmdHistory;
}

/// The core of the game.
void CliGame::play() {

    setUpPiles();

    std::vector<BasePile*> piles{tableau[0],tableau[1],tableau[2],
                                 tableau[3], tableau[4],tableau[5],tableau[6],
                                 foundations[0],foundations[1],foundations[2],
                                 foundations[3],wastePile};

    PlayingCard *activeCard{0};
    std::string buf, cmd;
    int x, y, z;
    while (getline(std::cin, buf)) {
        std::istringstream ss(buf);
        // get first word ~ command
        ss >> cmd;
        if (cmdList.find(cmd) == cmdList.end()) {
            continue;
        }

        switch (cmdList[cmd]) {
            case Exit:
                return;

            case Load: {
                std::string fname;
                ss >> fname;
                load(fname);
                break;
            }

            case Save: {
                std::string fname;
                ss >> fname;
                save(fname);
                break;
            }

            case Move: {
                x = -1; y = -1; z = -1;

                ss >> x >> y >> z;
                if (!(x >= 0 && x < 12 && y >= 0 && y <= 11 && x != y)) {
                    std::cerr << "\033[1;31m\nInvalid arguments, wrong number "
                                 "of piles.\033[0m\n\n";
                    break;
                }

                if (piles[x]->isEmpty()) {
                    std::cerr <<  "\033[1;31m\nInvalid move!\033[0m\n\n";
                    break;
                }
                activeCard = piles[x]->getLast();

                if (x < 7 && z < piles[x]->size() && z >= 0) {
                    activeCard = (*piles[x])[z];
                }

                // check if valid move
                if (isValidMove(activeCard, piles[y], y)) {
                    cmdHistory->execute(new MoveCommand<PlayingCard, BasePile>
                    (activeCard, piles[x], piles[y]) );
                }
                else {
                    std::cerr << "\033[1;31m\nInvalid move.\033[0m\n\n";
                }

                break;
            }

            case Uncover: {
                x = -1;
                ss >> x;
                if (x < 7 && x >= 0 && !piles[x]->isEmpty() &&
                    !piles[x]->getLast()->isTurnedUp())
                {
                    cmdHistory->execute(new ClickCommand<PlayingCard, BasePile>
                    (piles[x]->getLast(),0,0));
                }
                else {
                    std::cerr << "\033[1;31m\nCannot uncover card, invalid "
                                 "arguments.\033[0m\n\n";
                }
                break;
            }

            case Get:
                if (!stock->isEmpty() || !wastePile->isEmpty()) {
                    cmdHistory->execute(new ClickCommand<PlayingCard, BasePile>
                    (0,stock, wastePile) );
                }
                else {
                    std::cerr << "\033[1;31m\nCannot get another card, stock is"
                                 " empty.\033[0m\n\n";
                }
                break;

            case Print:
                displayCards();
                break;

            case HelpMove: {
                showPossibleMoves();
                break;
            }

            case Undo:
                if (!cmdHistory->isEmpty()) {
                    cmdHistory->undo();
                }
                else {
                    std::cerr << "\033[1;31m\nCannot undo.\033[0m\n\n";
                }
                break;

            case Serve:
                clear();
                setUpPiles();
                break;

            case Help:
            default:
                std::cout << helpStr;
                break;
        };

        if (checkWin()) {
            std::cout << "Would you like to continue? (press y if yes)\n";
            std::string cont;
            std::cin >> cont;
            if (cont == "y") {
                clear();
                setUpPiles();
            }
            else {
                break;
            }
        }

        cmd = "p";
    }
}

//static std::string cardToStr(int number, int color) {
static std::string cardToStr(PlayingCard *c) {
    static std::map<int, std::string> mn{{1, "A"}, { 2, "2"}, {3, "3"}, {4, "4"}
                                        ,{5, "5"}, { 6, "6"}, {7, "7"}, {8, "8"}
                                        ,{9, "9"}, {10, "10"}, {11,"J"},
                                         {12,"Q"}, {13,"K"}};
    static std::map<int, std::string> mc{{0, "♠"}, {1, "❤"}, {2, "◆"}, {3,"♣"}};
    return mn[c->getNumber()] + mc[c->getColor()];
}

void CliGame::displayCards() {
    int cnt = 0;
    for (int j = 0; j < 7; j++) {
        std::cout << cnt++ << ":  ";
        for (auto i : tableau[j]->getCards()) {
            if (i->isTurnedUp()) {
                std::cout << cardToStr(i);
            }
            else {
                std::cout << "##";
            }
            std::cout << "|";
        }
        std::cout << "\n\n";
    }
    std::cout << "\n-----------------\n";

    for (int i = 0; i < 4; i++) {
        if (cnt < 10) {
            std::cout << cnt++ << ":  ";
        }
        else {
            std::cout << cnt++ << ": ";
        }

        if (foundations[i]->isEmpty()) {
            std::cout << "|  |";
        }
        else {
            auto c = foundations[i]->getLast();
            std::cout << "|" << cardToStr(c) << "|";
        }
        std::cout << "\n";
    }
    std::cout << "\n-----------------\n";
    std::cout << cnt++ << ": ";
    if (!wastePile->isEmpty()) {
        auto c = wastePile->getLast();
        std::cout << "|" << cardToStr(c) << "| ";
    }
    else {
        std::cout << "|  | ";
    }

    if (stock->isEmpty()) {
        std::cout << "|  | ";
    }
    else {
        std::cout << "|##| ";
    }

    std::cout << "\n\n";
}

void CliGame::setUpPiles() {

    std::list<PlayingCard*> tmplist = shuffleCards();
    PlayingCard* card;

    for (int i = 0; i < 7; i++) {
        for (int j = 0; j <= i; j++) {
            card = tmplist.front();
            tmplist.pop_front();
            tableau[i]->addCard(card);
            card->setPile(tableau[i]);
        }
        card->turnUp();
    }

    while (!tmplist.empty()) {
        card = tmplist.front();
        tmplist.pop_front();
        stock->addCard(card);
        card->setPile(stock);
    }
}

void CliGame::clear() {

    for (auto i : cards) {
        i->setPile(0);
        i->turnDown();
    }

    for (int i = 0; i < 7; i++) {
        tableau[i]->clear();
    }

    for (int i = 0; i < 7; i++) {
        foundations[i]->clear();
    }

    stock->clear();
    wastePile->clear();

    delete cmdHistory;
    cmdHistory = new CommandManager<PlayingCard, BasePile>();
}

void CliGame::load(const std::string &fname) {
    clear();
    try {
        load_cards<std::list<PlayingCard*>, PlayingCard, BasePile>
        (cards, foundations, stock, wastePile, tableau, fname);
        // set card piles
        for (int i = 0; i < 7; i++) {
            for (auto card : tableau[i]->getCards()) {
                card->setPile(tableau[i]);
            }
        }

        for (int i = 0; i < 4; i++) {
            for (auto card : foundations[i]->getCards()) {
                card->setPile(foundations[i]);
                card->turnUp();
            }
        }

        for (auto card : stock->getCards()) {
            card->setPile(stock);
        }

        for (auto card : wastePile->getCards()) {
            card->setPile(wastePile);
        }
    }
    catch (...) {
        clear();
        setUpPiles();
        std::cerr << "\033[1;31m\nCannot load game.\033[0m\n\n";
    }
}

void CliGame::save(const std::string &fname) {
    try {
        save_cards<PlayingCard, BasePile>(foundations, stock, wastePile,
                                          tableau, fname);
    }
    catch (...) {
        std::cerr << "\033[1;31m\nCannot save game.\033[0m\n\n";
    }
}

std::list<PlayingCard*> CliGame::shuffleCards() {
    std::vector<PlayingCard*> ret{std::begin(cards), std::end(cards)};

    std::srand (unsigned(std::time(0)));
    std::random_shuffle (ret.begin(), ret.end(), [](int i){
        return (std::rand() + i) % 52; });

    return std::list<PlayingCard*> {ret.begin(), ret.end()};
}

bool CliGame::isValidMove(PlayingCard *card, BasePile *pile, int pos) const {

    // tableau
    if (pos < 7) {

        if (pile->isEmpty()) {
            return card->getNumber() == 13;
        }
        else {
            PlayingCard *lastCard = pile->getLast();
            return (lastCard->isTurnedUp() && card->isTurnedUp() &&
                    card->getNumber() + 1 == lastCard->getNumber() &&
                    card->isRed() != lastCard->isRed());
        }

    }
    // foundations
    else if (pos < 11) {

        if (pile->isEmpty()) {
            return  card == card->getPile()->getLast() && card->isTurnedUp() &&
                    card->getNumber() == ACE;
        }
        else {
            PlayingCard *lastCard = pile->getLast();
            return (card == card->getPile()->getLast() && card->isTurnedUp() &&
                    card->getColor() == lastCard->getColor() &&
                    card->getNumber() == lastCard->getNumber() + 1);
        }

    }

    return false;
}

bool CliGame::checkWin() const {
    if (foundations[0]->size() == 13 &&
        foundations[1]->size() == 13 &&
        foundations[2]->size() == 13 &&
        foundations[3]->size() == 13)
    {
        std::cout << "Congratulations, you have won the game!\n";
        return true;
    }
    return false;
}

void CliGame::showPossibleMoves() {

    std::vector<std::pair<PlayingCard*, BasePile*>> cmds =
    helpAction<PlayingCard, BasePile>(wastePile, foundations, tableau);

    std::string from{};
    std::string to{};

    if (cmds.empty()) {
        std::cout << "Get another card from stock!\n";
    }

    for (auto i : cmds) {
        if (i.second) {
            std::cout << cardToStr(i.first) << " -> ";
            std::cout << "[" << i.second->getPos() << "]";
            if (!i.second->isEmpty()) {
                std::cout << " " << cardToStr(i.second->getLast());
            }
            std::cout << "\n";
        }
        else {
            std::cout << "Uncover: " <<  i.first->getPile()->getPos() << "\n";
        }
    }
}
