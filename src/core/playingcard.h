/// Pasians
/// ICP FIT VUT Brno 2017
///
/// @author Jakub Semrič, xsemri00@stud.fit.vutbr.cz
/// @author Peter Rusiňák, xrusin03@stud.fit.vutbr.cz
///
/// Definition of class @PlayingCard.
/// @file playingcard.h
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0
///

#ifndef PLAYINGCARD_H
#define PLAYINGCARD_H

#define KING 13
#define ACE 1

class BasePile;

/// Class which stands for a card as in real world.
class PlayingCard {

    public:
        /// possible card colors
        enum {SPADE = 0, HEART, DIAMOND, CLUB};

    private:
        /// card color
        int color;
        /// 0-special,1-A,2,3,4,5,6,7,8,9,10,11-J,12-Q,13-K
        unsigned char number;
        /// visible or not
        bool turnedUp;
        /// actual pile where card is located
        BasePile *basePile;


    public:
        PlayingCard(int cl, unsigned char num, bool rev);
        virtual ~PlayingCard() = default;

        bool isTurnedUp() const;
        unsigned char getNumber() const;
        int getColor() const;
        bool isRed() const;
        virtual void turnUp();
        virtual void turnDown();
        void moveTo(BasePile*);
        BasePile *getPile() const;
        void setPile(BasePile *);
};

#endif
