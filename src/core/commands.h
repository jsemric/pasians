/// Pasians
/// ICP FIT VUT Brno 2017
///
/// @author Jakub Semrič, xsemri00@stud.fit.vutbr.cz
/// @author Peter Rusiňák, xrusin03@stud.fit.vutbr.cz
///
/// Definition of operations represented by specific classes.
/// @file commands.h
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0
///

#ifndef COMMAND_H
#define COMMAND_H

#include <iostream>
#include <vector>

/// Abstract class representing an operation in a game.
template <class TypeCard, class TypePile>
class Command {
    protected:
        /// card which was moved or turned up
        TypeCard *card;
        /// pile where card was placed before action
        TypePile *src;
        /// pile where card was placed after action
        TypePile *dst;

    public:
        Command(TypeCard *card, TypePile *srcPile, TypePile *dstPile);
        virtual ~Command();
        virtual void execute() = 0;
        virtual void undo() = 0;
};


/// Representation of any move action in a game.
template <class TypeCard, class TypePile>
class MoveCommand : public Command<TypeCard, TypePile> {
    public:
        MoveCommand(TypeCard *card, TypePile *srcPile, TypePile *dstPile);
        ~MoveCommand() {};

        void execute() override;
        void undo() override;
};

/// Representation of any other action different from move.
template <class TypeCard, class TypePile>
class ClickCommand : public Command<TypeCard, TypePile> {
    public:
        ClickCommand(TypeCard *card, TypePile *srcPile, TypePile *dstPile);
        ~ClickCommand() {};

        void execute() override;
        void undo() override;
};

template<class TypeCard, class TypePile> Command<TypeCard, TypePile>::Command
(TypeCard *crd, TypePile *srcPile, TypePile *dstPile) :
    card{crd}, src{srcPile}, dst{dstPile}
{

}

template<class TypeCard, class TypePile>
Command<TypeCard, TypePile>::~Command() {}


template<class TypeCard, class TypePile>
MoveCommand<TypeCard, TypePile>::MoveCommand(TypeCard *crd, TypePile *srcPile,
TypePile *dstPile): Command<TypeCard, TypePile>{crd, srcPile, dstPile}
{

}

/// Provides move operation, so cards are placed to a new pile.
template<class TypeCard, class TypePile>
void MoveCommand<TypeCard, TypePile>::execute() {
    this->card->moveTo(this->dst);
}

/// Provides an undo move operation, so cards are placed back
/// to a previous pile.
template<class TypeCard, class TypePile>
void MoveCommand<TypeCard, TypePile>::undo() {
    this->card->moveTo(this->src);
}

template<class TypeCard, class TypePile>
ClickCommand<TypeCard, TypePile>::ClickCommand
(TypeCard *crd, TypePile *srcPile, TypePile *dstPile):
    Command<TypeCard, TypePile>{crd, srcPile, dstPile}
{

}

/// Provides a click operation so either the card is being turned up
/// or card from a stock is moved to a waste.
template<class TypeCard, class TypePile>
void ClickCommand<TypeCard, TypePile>::execute() {
    if (this->src) {
        if (this->src->isEmpty()) {
            while (!this->dst->isEmpty()) {
                TypeCard *topCard = this->dst->getLast();
                topCard->turnDown();
                topCard->moveTo(this->src);
            }
        }
        else {
            TypeCard *topCard = this->src->getLast();
            topCard->turnUp();
            topCard->moveTo(this->dst);
        }
    }
    else {
        this->card->turnUp();
    }
}

/// Provides opposite action as member function @execute.
template<class TypeCard, class TypePile>
void ClickCommand<TypeCard, TypePile>::undo() {
    if (this->dst) {
        if (this->dst->isEmpty()) {
            while (!this->src->isEmpty()) {
                TypeCard *topCard = this->src->getLast();
                topCard->turnUp();
                topCard->moveTo(this->dst);
            }
        }
        else {
            TypeCard *topCard = this->dst->getLast();
            topCard->turnDown();
            topCard->moveTo(this->src);
        }
    }
    else {
        this->card->turnDown();
    }
}

template <class TypeCard, class TypePile>
std::vector<std::pair<TypeCard*, TypePile*>> helpAction
(TypePile *wastePile, TypePile *foundations[4], TypePile *tableau[7]);


/// Provides a possible move which can be executed. The core of the algorithm
/// lies in breadth first search.
template <class TypeCard, class TypePile>
std::vector<std::pair<TypeCard*, TypePile*>> helpAction
(TypePile *wastePile, TypePile *foundations[4], TypePile *tableau[7])
{

    std::vector<std::pair<TypeCard*, TypePile*>> ret{};
    // uncover all uncovered cards
    for (int i = 0; i < 7; i++) {
        if (tableau[i]->isEmpty()) {
            continue;
        }

        TypeCard *deckCard = tableau[i]->getLast();
        // found uncovered card
        if (!deckCard->isTurnedUp()) {
            ret.push_back(std::pair<TypeCard*, TypePile*>(deckCard, 0));
        }
    }

    // first uncover all cards
    if (!ret.empty()) {
        return ret;
    }

    // find if it is possible to move some card to foundations
    TypeCard *card = 0;
    if (!wastePile->isEmpty()) {
        card = wastePile->getLast();
    }

    for (int i = 0; i < 4; i++) {

        // card from waste to foundations
        if (card) {
            if (foundations[i]->isEmpty()) {
                if (card->getNumber() == 1) {
                    ret.push_back(std::pair<TypeCard*, TypePile*>
                    (card, foundations[i]));
                }
            }
            else {
                TypeCard *fLast = foundations[i]->getLast();
                if (card->getColor() == fLast->getColor() &&
                    card->getNumber() == fLast->getNumber() + 1)
                {
                    ret.push_back(std::pair<TypeCard*, TypePile*>
                    (card, foundations[i]));
                }
            }
        }

        // card from card decks
        for (int j = 0; j < 7; j++) {

            if (tableau[j]->isEmpty()) {
                continue;
            }

            TypeCard *deckLast = tableau[j]->getLast();
            if (foundations[i]->isEmpty()) {
                if (deckLast->getNumber() == 1) {
                    ret.push_back(std::pair<TypeCard*, TypePile*>
                    (deckLast, foundations[i]));
                }
            }
            else {
                TypeCard *fLast = foundations[i]->getLast();
                if (deckLast->getColor() == fLast->getColor() &&
                    deckLast->getNumber() == fLast->getNumber() + 1)
                {
                    ret.push_back (std::pair<TypeCard*, TypePile*>
                    (deckLast, foundations[i]));
                }
            }
        }
    }

    // any attempt to place card to foundations failed

    for (int i = 0; i < 7; i++) {
        if (tableau[i]->isEmpty()) {
            continue;
        }

        bool firstUnc = false;
        for (auto card1 : tableau[i]->getCards()) {

            // search just for first visible card
            if (!card1->isTurnedUp()) {
                firstUnc = true;
                continue;
            }


            for (int j = 0; j < 7; j++) {

                if (i == j) {
                    continue;
                }

                // place king to empty slot
                if (tableau[j]->isEmpty()) {
                    if (card1->getNumber() == 13 && firstUnc)
                    {
                        ret.push_back(std::pair<TypeCard*, TypePile*>
                        (card1, tableau[j]));
                    }
                }
                else {
                    // place card in order to gain access to uncovered card
                    TypeCard *last = tableau[j]->getLast();
                    if (firstUnc && card1->getNumber() + 1 == last->getNumber()
                        && last->isRed() != card1->isRed())
                    {
                        ret.push_back(std::pair<TypeCard*, TypePile*>
                        (card1, tableau[j]));
                    }
                }

            }
            // no more first uncovered card
            break;
        }
    }

    // place card from waste to card decks
    if (card) {
        for (int i = 0; i < 7; i++) {
            if (tableau[i]->isEmpty()) {
                if (card->getNumber() == 13) {
                    ret.push_back(
                    std::pair<TypeCard*, TypePile*>(card, tableau[i]));
                }
                continue;
            }
            TypeCard *last = tableau[i]->getLast();
            if (card->getNumber() + 1 == last->getNumber() &&
                last->isRed() != card->isRed())
            {
                ret.push_back(
                std::pair<TypeCard*, TypePile*>(card, tableau[i]));
            }
        }
    }

    return ret;
}

#endif
