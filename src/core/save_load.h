/// Pasians
/// ICP FIT VUT Brno 2017
///
/// @author Jakub Semrič, xsemri00@stud.fit.vutbr.cz
/// @author Peter Rusiňák, xrusin03@stud.fit.vutbr.cz
///
/// Implementation of save load functions.
/// @file save_load.h
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0
///

#ifndef SAVE_LOAD_H
#define SAVE_LOAD_H

#include <fstream>
#include <map>
#include <vector>
#include <sstream>

template <class TypeCardList, class TypeCard, class TypePile>
void load_cards(TypeCardList &cards, TypePile *foundations[4], TypePile *stock,
                TypePile *wastePile, TypePile *tableau[7],
                const std::string &fname);


template <class TypeCard, class TypePile>
void save_cards(TypePile *foundations[4], TypePile *stock,
                TypePile *wastePile, TypePile *tableau[7],
                const std::string &fname);

/// Saves the actual state of game.
template <class TypeCard, class TypePile>
void save_cards(TypePile *foundations[4], TypePile *stock,
                TypePile *wastePile, TypePile *tableau[7],
                const std::string &fname)
{
    std::ofstream output(fname);
    if (!output.is_open()) {
        throw 1;
    }

    for (auto i : stock->getCards()) {
        output << (int)i->getNumber()*4 + i->getColor() << " ";
    }
    output << "\n";

    for (auto i : wastePile->getCards()) {
        output << (int)i->getNumber()*4 + i->getColor() << " ";
    }
    output << "\n";


    for (int j = 0; j < 4; j++) {
        for (auto i : foundations[j]->getCards()) {
            output << (int)i->getNumber()*4 + i->getColor() << " ";
        }
        output << "\n";
    }

    for (int j = 0; j < 7; j++) {
        bool t = true;
        for (auto i : tableau[j]->getCards()) {
            if (t && i->isTurnedUp()) {
                t = false;
                // zero marks the point from where all cards are visible
                output << "0 ";
            }
            output << (int)i->getNumber()*4 + i->getColor() << " ";
        }
        output << "\n";
    }

    output.close();
}

/// Loads a game from a file.
template <class TypeCardList, class TypeCard, class TypePile>
void load_cards(TypeCardList &cards, TypePile *foundations[4], TypePile *stock,
                TypePile *wastePile, TypePile *tableau[7],
                const std::string &fname)
{
    std::ifstream input(fname);
    if (!input.is_open()) {
        throw 1;
    }

    std::string buf;
    std::map<int, TypeCard*> cardMap;
    for (auto i : cards) {
        int hash = i->getColor() + i->getNumber()*4;
        cardMap[hash] = i;
    }

    // stock, waste, foundations
    std::vector<TypePile*> piles{stock, wastePile, foundations[0],
                                 foundations[1], foundations[2],
                                 foundations[3]};

    for (int i = 0; i < 6; i++) {
        if (getline(input, buf)) {
            if (buf == "\n") {
                continue;
            }
            std::istringstream ss{buf};
            int x;
            while (ss >> x) {
                if (cardMap.at(x)) {
                    piles[i]->addCard(cardMap[x]);
                    cardMap[x] = 0;
                }
                else {
                    input.close();
                    throw 1;
                }
            }
        }
        else {
            input.close();
            throw 1;
        }
    }

    // tableau
    for (int i = 0; i < 7; i++) {
        if (getline(input, buf)) {
            if (buf == "\n") {
                continue;
            }
            std::istringstream ss{buf};
            int x;
            bool t = false;
            while (ss >> x) {

                if (x == 0) {
                    t = true;
                    continue;
                }

                if (cardMap.at(x)) {
                    tableau[i]->addCard(cardMap[x]);
                    if (t) {
                        cardMap[x]->turnUp();
                    }
                    cardMap[x] = 0;
                }
                else {
                    input.close();
                    throw 1;
                }
            }
        }
        else {
            input.close();
            throw 1;
        }
    }
    for (auto i : cardMap) {
        if (i.second != 0) {
            throw 1;
        }
    }

    input.close();
}

#endif
