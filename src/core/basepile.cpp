/// Pasians
/// ICP FIT VUT Brno 2017
///
/// @author Jakub Semrič, xsemri00@stud.fit.vutbr.cz
/// @author Peter Rusiňák, xrusin03@stud.fit.vutbr.cz
///
/// Implementation if the Basepile class member functions.
/// @file basepile.cpp
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0
///

#include "basepile.h"
#include "playingcard.h"
#include <list>
#include <algorithm>

BasePile::BasePile(int p): pos{p}
{

}

/// Inserts a card to a pile.
void BasePile::addCard(PlayingCard *card) {
    cards.push_back(card);
}

/// Access to a card as to an element in array by index.
/// The lowest card is located at index 0.
PlayingCard *BasePile::operator[](int pos) const {
    auto it = cards.begin();
    std::advance(it, pos);
    return *it;
}

/// Removes a specific card in pile.
void BasePile::removeCard(PlayingCard *card) {
    for (auto i = cards.begin(); i != cards.end(); i++) {
        if (*i == card) {
            cards.erase(i);
        }
    }
}

/// Returns a top card in pile.
PlayingCard *BasePile::getLast() const {
    return cards.back();
}

/// Check whether pile contains no cards.
bool BasePile::isEmpty() const {
    return cards.size() == 0;
}

/// Returns a list of all cards.
std::list<PlayingCard*> BasePile::getCards() const {
    return cards;
}

/// Moves a card and it's upper cards from one pile to another.
void BasePile::splice(BasePile *src, PlayingCard *card) {
    auto i = std::find(src->cards.begin(), src->cards.end(), card);
    for (auto c = i; c != src->cards.end(); c++) {
        (*c)->setPile(this);
    }

    if (i != cards.end()) {
        cards.splice(cards.end(), src->cards, i, src->cards.end());
    }
}

/// Returns a number of cards in pile.
int BasePile::size() const {
    return cards.size();
}

/// Returns a position of pile in a CliGame.
int BasePile::getPos() const {
    return pos;
}

/// Removes all cards from pile.
void BasePile::clear() {
    cards.clear();
}
