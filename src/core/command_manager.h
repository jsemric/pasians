/// Pasians
/// ICP FIT VUT Brno 2017
///
/// @author Jakub Semrič, xsemri00@stud.fit.vutbr.cz
/// @author Peter Rusiňák, xrusin03@stud.fit.vutbr.cz
///
/// Contains a definition of class CommandMannger which manages
/// game commands.
/// @file command_manger.h
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0
///

#ifndef COMMAND_MANAGER_H
#define COMMAND_MANAGER_H

#include <vector>

template<class TypeCard, class TypePile> class Command;

/// Template class for managing game moves.
template<class TypeCard, class TypePile>
class CommandManager {
        /// Vector of executed commands, mainly used for undo actions.
        std::vector<Command<TypeCard, TypePile>*> undoStack;

    public:
        CommandManager();
        ~CommandManager();
        void execute(Command<TypeCard, TypePile> *cmd);
        void undo();
        bool isEmpty();
};

/// Constructs a CommandManager object.
template <class TypeCard, class TypePile>
CommandManager<TypeCard, TypePile>::CommandManager() {

}

/// Checks whether undoStack is empty.
template <class TypeCard, class TypePile>
bool CommandManager<TypeCard, TypePile>::isEmpty() {
    return undoStack.empty();
}

/// Removes an object.
template <class TypeCard, class TypePile>
CommandManager<TypeCard, TypePile>::~CommandManager() {
    while (undoStack.size() != 0) {
        delete undoStack.back();
        undoStack.pop_back();
    }
}

/// Executes an operation and adds it to undoStack.
template <class TypeCard, class TypePile>
void CommandManager<TypeCard, TypePile>::execute
(Command<TypeCard, TypePile> *cmd) {
    cmd->execute();
    undoStack.push_back(cmd);
}

/// Executes an undo operations and removes last item in undoStack.
template <class TypeCard, class TypePile>
void CommandManager<TypeCard, TypePile>::undo() {
    undoStack.back()->undo();
    delete undoStack.back();
    undoStack.pop_back();
}

#endif
