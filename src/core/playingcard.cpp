/// Pasians
/// ICP FIT VUT Brno 2017
///
/// @author Jakub Semrič, xsemri00@stud.fit.vutbr.cz
/// @author Peter Rusiňák, xrusin03@stud.fit.vutbr.cz
///
/// Implementation of member functions of class @PlayingCard.
/// @file playingcard.cpp
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0
///

#include "playingcard.h"
#include "basepile.h"

#define TOTAL_CARDS 52

PlayingCard::PlayingCard(int cl, unsigned char num, bool rev):
    color{cl}, number{num}, turnedUp{rev} {}

/// Sets @PlayingCard visibility flag to true.
void PlayingCard::turnUp() {
    turnedUp = true;
}

/// Sets @PlayingCard visibility flag to false.
void PlayingCard::turnDown() {
    turnedUp = false;
}

/// Check whether card color is red.
bool PlayingCard::isRed() const {
    return color == PlayingCard::DIAMOND ||
           color == PlayingCard::HEART;
}

/// Checks whether card is visible or not.
bool PlayingCard::isTurnedUp() const {
    return turnedUp;
}

/// Return a number of the card.
unsigned char PlayingCard::getNumber() const {
    return number;
}

/// Return a color of the card.
int PlayingCard::getColor() const {
    return color;
}

/// Return a pile where card is located.
BasePile *PlayingCard::getPile() const {
    return basePile;
}

/// Moves from actual pile to another one.
void PlayingCard::moveTo(BasePile *pile) {
    pile->splice(basePile, this);
}

/// Sets a new pile.
void PlayingCard::setPile(BasePile *pile) {
    basePile = pile;
}
