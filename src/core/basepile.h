/// Pasians
/// ICP FIT VUT Brno 2017
///
/// @author Jakub Semrič, xsemri00@stud.fit.vutbr.cz
/// @author Peter Rusiňák, xrusin03@stud.fit.vutbr.cz
///
/// Declaration if piles which contain cards.
/// @file basepile.h
///
/// Unless otherwise stated, all code is licensed under a
/// GNU General Public Licence v2.0
///

#ifndef CARDDECK_H
#define CARDDECK_H

#include <list>

class PlayingCard;

/// Representation of pile which basically contains some cards.
class BasePile {

private:
    /// List of cards.
    std::list<PlayingCard*> cards;
    /// position in CliGame
    int pos;

public:
    BasePile(int p);

    void addCard(PlayingCard *);
    void removeCard(PlayingCard *);
    PlayingCard *getLast() const;
    bool isEmpty() const;
    std::list<PlayingCard*> getCards() const;
    PlayingCard *operator[](int pos) const;
    void splice(BasePile *, PlayingCard *);
    int size() const;
    int getPos() const;
    void clear();
};

#endif
