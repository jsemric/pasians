#!/usr/bin/env python3

from subprocess import Popen, PIPE

with open("tests.txt") as f:
    for test in f.read().split("\n\n"):
        name, data = test.split("\n", 1)
        human_name, filename = name.replace(')', '').split(' (')
        data = 'load savegame\n' + data
        
        p = Popen(['../hra2017-cli'], stdin=PIPE, stdout=PIPE, stderr=PIPE)
        out, _ = p.communicate(bytes(data, 'utf-8'))
        out_str = out.decode("utf-8")
        
        
        result = ' ?? '
        
        try:
            with open('ref-out/' + filename) as ref_f:
                result = ' OK ' if ref_f.read() == out_str else 'FAIL'
        except FileNotFoundError:
            result = 'NEW '
            with open('ref-out/' + filename + '.new', 'w') as ref_f:
                ref_f.write(out_str)
        
        print(human_name.ljust(74) + '[' + result + ']')
        
        if result != ' OK ':
            print(out_str, end='')
