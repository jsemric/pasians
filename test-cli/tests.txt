Tisk nápovědy (help)
help

Vypsání karet (print)
print

Otočení karty (uncover)
uncover 2
print

Otočení již otočené karty (uncover)
uncover 0
uncover 1
uncover 2
uncover 3
uncover 4
uncover 5
uncover 6
uncover 7
uncover 8
uncover 9
uncover 10
uncover 11
print

Otočení karty se špatnými parametry (print)
uncover
u
uncover 20
uncover abc
print

Otočení prázdné hromádky (uncover-2)
get
get
get
get
get
get
get
get
move 11 1
move 0 1
uncover 0
print

Přesun karat (move)
uncover 2
move 6 2
print
move 2 6
print

Nedovolené přesuny (move-invalid)
move 3 4
move 5 0
move 2 2
move 8 8
move 9 10
move 11 1
move
move 1
move 1 2 3
move a
print

Přesun mezi hromádkami, nedovolený přesun zpět (move-2)
move 7 8
move 8 5
print

Přesun na hromádku a zpět (move-3)
get
move 11 7
move 11 2
get
get
get
get
get
get
get
get
get
get
get
move 11 0
move 7 0
print

Get - konec hromádky (get)
get
get
get
get
get
get
get
get
get
get
get
get
get
get
get
get
get
get
get
get
print
get
print
get
print
get
print
get
print
get
print
get
print

Vracení tahů (undo)
uncover 2
undo
print
get
undo
print
get
helpmove
move 3 4
undo
print
uncover 2
move 6 2
uncover 2
help
undo
print
undo
print

Nápověda tahu (helpmove)
helpmove
uncover 2
helpmove
get
helpmove
move 11 7
helpmove
get
get
get
get
helpmove
get
helpmove
get
helpmove
print

Uložení a načítání hry (save)
uncover 2
get
save test-save
move 11 7
get
get
get
get
get
get
move 11 10
load test-save
print
load neexistujici-soubor
print
load ref-out/get
print
load necitelny-soubor
print

Zkrácené varianty příkazů (short-commands)
s test-save
c 2
m 6 2
g
h
hm
p
u
p
l test-save
p
e
p

Celá hra (full-game)
c 2
g
m 11 7
g
g
g
g
g
g
m 11 9
g
m 11 1
m 0 1
g
m 11 0
m 4 0
c 4
g
g
g
m 11 1
m 5 1
c 5
m 5 8
m 1 8
c 5
g
m 11 3
m 6 3 5
c 6
g
g
g
m 11 3
g
m 11 7
g
g
g
m 11 4
m 1 4 1
c 1
m 11 1
m 3 1 3
c 3
m 3 8
c 3
g
g
m 11 2
g
g
g
g
g
g
m 11 8
g
m 11 5
g
g
g
m 11 1
m 5 1 3
c 5
m 5 6
c 5
m 5 2
c 5
m 5 0
m 6 5 4
c 6
g
m 11 0
g
g
g
g
m 11 8
g
g
g
g
g
m 11 0
m 4 0 3
c 4
m 4 10
m 3 10
c 3
c 4
m 4 2
c 4
m 4 9
m 0 9
m 0 7
g
m 11 3
g
g
g
m 11 10
m 1 10
m 1 7
m 3 10
m 2 10
m 0 3
m 0 7
m 0 10
m 6 0
c 6
m 6 4
c 6
m 6 5
c 6
m 6 9
m 3 9
m 3 8
m 1 9
m 0 9
m 1 8
m 2 7
m 0 7
m 1 10
m 2 9
m 1 7
m 2 1
c 2
m 0 9
m 2 10
g
m 11 9
g
g
m 11 8
g
m 11 9
g
m 1 8
m 0 8
m 1 10
m 0 9
m 5 10
m 11 10
m 11 7
m 1 7
m 1 9
m 5 7
g
m 11 8
m 0 8
m 0 9
m 1 7
m 4 8
m 5 10
