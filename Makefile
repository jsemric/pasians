PROJ=hra2017
PROJ_CLI=$(PROJ)-cli
CXX=g++
CCFLAGS=-std=c++11 -Wall -Wextra -pedantic
QMAKE=qmake
DOX=doxygen
ARCHIVE=xsemri00-xrusin03.zip
HOST=$(shell hostname)

ifeq ("$(wildcard /usr/bin/qmake)","")
QMAKE=qmake-qt5
endif

ifeq ("$(HOST)","merlin.fit.vutbr.cz")
ifeq ("$(wildcard /usr/bin/qmake-eq5)","")
QMAKE=/usr/local/share/Qt-5.5.1/5.5/gcc_64/bin/qmake
export LD_LIBRARY_PATH:=/usr/local/share/Qt-5.5.1/5.5/gcc_64/lib:${LD_LIBRARY_PATH}
export QT_PLUGIN_PATH:=/usr/local/share/Qt-5.5.1/5.5/gcc_64/plugins:${QT_PLUGIN_PATH}
endif
endif

SRC_PATH=$(shell pwd)/src
CORE_PATH=$(SRC_PATH)/core
CLI_PATH=$(SRC_PATH)/cli
GUI_PATH=$(SRC_PATH)/gui

CORE_SRC= $(wildcard $(CORE_PATH)/*.cpp)
CORE_HEAD= $(wildcard $(CORE_PATH)/*.h)
CORE_OBJ= $(patsubst %.cpp, %.o, $(CORE_SRC))

CLI_SRC= $(wildcard $(CLI_PATH)/*.cpp)
CLI_HEAD= $(wildcard $(CLI_PATH)/*.h)
CLI_OBJ= $(patsubst %.cpp, %.o, $(CLI_SRC))

GUI_SRC= $(wildcard $(GUI_PATH)/*.cpp)
GUI_HEAD= $(wildcard $(GUI_PATH)/*.h)
GUI_OBJ= $(patsubst %.cpp, %.o, $(GUI_SRC))

.PHONY: clean all cli doxygen pack run run-gui

all: $(PROJ) $(PROJ_CLI)

gui: $(PROJ)

cli: $(PROJ_CLI)

$(PROJ): $(CORE_OBJ) $(GUI_SRC) $(GUI_HEAD)
	@cd $(GUI_PATH) && $(QMAKE) && make
	@mv $(GUI_PATH)/$(PROJ) .

$(PROJ_CLI): $(CORE_OBJ) $(CLI_OBJ)
	$(CXX) $(CCFLAGS) $^ -o $@

$(CLI_PATH)/main.o: $(CLI_PATH)/main.cpp
	$(CXX) $(CCFLAGS) -c $< -o $@

%.o: %.cpp %.h
	$(CXX) $(CCFLAGS) -c $< -o $@

doxygen:
	$(DOX) $(SRC_PATH)/doxyConf

clean:
	-@cd $(GUI_PATH) && make clean
	rm -f $(PROJ_CLI) $(PROJ) $(CORE_PATH)/*.o $(CLI_PATH)/*.o
	rm -f $(GUI_PATH)/Makefile
	rm -f $(ARCHIVE)
	rm -rf doc

pack: clean
	zip -r $(ARCHIVE) src/ examples/ Makefile README.md

run: all
	(./$(PROJ) &) && ./$(PROJ_CLI)

run-gui: all
	./$(PROJ)
