ICP 2017 - Pasians
==================

Game solitaire for ICP project VUT FIT Brno, 2017.

No points, no timer, just patience.

How to play
===========
`make run` for run cli and normal game

Game rules:
https://en.wikipedia.org/wiki/Klondike_%28solitaire%29
